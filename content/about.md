---
title: "About"
---
Hi, I'm Jack Chan. a Backend developer / DevOps Engineer who live in Hong Kong. Mainly use node.js and go programming language in workspace. This blog is powered by [Hugo](https://gohugo.io/) and [anatole theme](https://github.com/lxndrblz/anatole).  

If you’d like to contact me, please [send me an email](mailto:awcjack@protonmail.com).  
If there are typo or mistake in post, you can send me email or [create issue/merge request in gitlab](https://gitlab.com/awcjack/hugo-blog).