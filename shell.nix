{ pkgs ? import (builtins.fetchTarball {
  url = "https://github.com/nixos/nixpkgs/archive/292daa1285c888e110af4d03ce544b9d2963b9aa.tar.gz";
}) {} }:

pkgs.mkShell {
    buildInputs = [ 
        pkgs.go_1_18
        pkgs.hugo
    ];
}